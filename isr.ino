// динамическая индикация в прерывании таймера 2
ISR(TIMER2_COMPA_vect) {
  indiCounter[curIndi]++;             // счётчик индикатора
  
  if (indiDimm[curIndi] != 0 && indiCounter[curIndi] >= indiDimm[curIndi]) { // если достигли порога диммирования
    #if (BOARD_TYPE == 4)
      setPin(LATCH, LOW);
      shiftOut(DATA, CLOCK, MSBFIRST, 0b00000000);
      shiftOut(DATA, CLOCK, MSBFIRST, 0b00000000);
      setPin(LATCH, HIGH);
    #else
      setPin(opts[curIndi], 0);         // выключить текущий индикатор
    #endif
  }
    
  if (indiCounter[curIndi] > 25) {    // достигли порога в 25 единиц
    indiCounter[curIndi] = 0;         // сброс счетчика лампы
    if (++curIndi >= 4) curIndi = 0;  // смена лампы закольцованная

    // отправить цифру из массива indiDigits согласно типу лампы
    if (indiDimm[curIndi] > 0) {
      
      #if (BOARD_TYPE == 4)
        byte thisDigArr[2] = {digitMask[indiDigits[curIndi]][0], digitMask[indiDigits[curIndi]][1]};
        setPin(LATCH, LOW);
        
        if (anodeStates[curIndi] && indiDimm[curIndi] != 0) {
          shiftOut(DATA, CLOCK, MSBFIRST, thisDigArr[0] + opts[curIndi]);
        } else {
          shiftOut(DATA, CLOCK, MSBFIRST, thisDigArr[0]);
        }
        
        shiftOut(DATA, CLOCK, MSBFIRST, thisDigArr[1]);
        setPin(LATCH, HIGH);
      #else  
        byte thisDig = digitMask[indiDigits[curIndi]];
        setPin(DECODER3, bitRead(thisDig, 0));
        setPin(DECODER1, bitRead(thisDig, 1));
        setPin(DECODER0, bitRead(thisDig, 2));
        setPin(DECODER2, bitRead(thisDig, 3));
        setPin(opts[curIndi], anodeStates[curIndi]);    // включить анод на текущую лампу
      #endif
    }
  }
}

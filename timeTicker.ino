byte prevMins = 0;
byte prevHrs = 0;

void calculateTime() {
  dotFlag = !dotFlag;
  
  if (dotFlag) {
    dotBrightFlag = true;
    dotBrightDirection = true;
    dotBrightCounter = 0;
  
    DateTime now = rtc.now();       // синхронизация с RTC
    secs = now.second();
    mins = now.minute();
    hrs = now.hour();
  
    if (prevMins != mins || prevHrs != hrs) {
      newTimeFlag = true;
      prevMins = mins;
      prevHrs = hrs;

      if (mins % BURN_PERIOD == 0) burnIndicators();     // чистим чистим!
      
      setNewTime();
      changeBright();
    }
  }
}
